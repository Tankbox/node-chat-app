class Users {
  constructor() {
    this.users = [];
  }

  addUser(id, name, room) {
    var user = {id, name, room};
    this.users.push(user);
    return user;
  }

  removeUser(id) {
    var index = this.users.findIndex((user) => user.id === id);
    return (index === -1) ? undefined : this.users.splice(index, 1).pop();
  }

  findUser(id) {
    return this.users.find((user) => user.id === id);
  }

  findUsersByRoom(room) {
    var users = this.users.filter((user) => user.room === room);
    var names = users.map((user) => user.name);
    return names;
  }

}

module.exports = {Users};
