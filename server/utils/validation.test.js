var expect = require('expect');
var {isRealString} = require('./validation');

describe('isRealString', () => {
  it('should reject non-string values', () => {
    var input = 123;
    expect(isRealString(input)).toBeFalsy();

    var input = undefined;
    expect(isRealString(input)).toBeFalsy();
  });

  it('should reject string with only spaces', () => {
    var input = '        ';
    expect(isRealString(input)).toBeFalsy();
  });

  it('should allow strings with non-space characters', () => {
    var input = '  this should pass      ';
    expect(isRealString(input)).toBeTruthy();

    var input = 'happy path';
    expect(isRealString(input)).toBeTruthy();
  });
});
