var expect = require('expect');
var {generateMessage, generateLocationMessage} = require('./message');

describe('generateMessage', () => {
  it('should generate correct message object', () => {
    var from = 'Expect Test User';
    var text = 'This is a unit test';
    var message = generateMessage(from, text);

    expect(message.from).toBe(from);
    expect(message.text).toBe(text);
    expect(typeof message.createdAt).toBe('number');
  });
});

describe('generateLocationMessage', () => {
  it('should generate correct location object', () => {
    var from = 'Expect Test User';
    var coords = {
      latitude: 30.267153,
      longitude: -97.743061
    }
    var expectedUrl = `https://www.google.com/maps?q=${coords.latitude},${coords.longitude}`
    var message = generateLocationMessage(from, coords);

    expect(message.from).toBe(from);
    expect(message.url).toBe(expectedUrl);
    expect(typeof message.createdAt).toBe('number');
  });
});
