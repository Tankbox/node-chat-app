var expect = require('expect');
var {Users} = require('./users');

describe('Users', () => {

  beforeEach(() => {
    users = new Users();
    users.users = [{
      id: '1',
      name: 'Mike',
      room: 'Node'
    },{
      id: '2',
      name: 'Nick',
      room: 'Node'
    },{
      id: '3',
      name: 'Jane',
      room: 'Spring'
    }];
  });

  it('should add a new user', () => {
    var users = new Users();
    var user = {
      id: '123',
      name: 'Joe',
      room: 'Nerd Ballas'
    }
    var result = users.addUser(user.id, user.name, user.room);

    expect(users.users).toEqual([user]);
  });

  it('should return names by room', () => {
    var usersByRoom = users.findUsersByRoom('Node');
    expect(usersByRoom).toEqual(['Mike', 'Nick']);

    var usersByRoom = users.findUsersByRoom('Spring');
    expect(usersByRoom).toEqual(['Jane']);
  });

  it('should remove a user by id', () => {
    var userToRemove = users.users[0];
    var removedUser = users.removeUser('1');
    expect(removedUser).toEqual(userToRemove);
  });

  it('should not remove a user by id', () => {
    var removedUser = users.removeUser('100');
    expect(removedUser).toBeUndefined();
  });

  it('should find a user by id', () => {
    var user = users.findUser('1');
    expect(user).toEqual(users.users[0]);
  });

  it('should not find a user by id', () => {
    var user = users.findUser('100');
    expect(user).toBeUndefined();
  });

});
